import numpy as np
import matplotlib.pyplot as plt

# Parámetros de la simulación
temperatura = 10.0
nParticulas = 1000
nPasos = 250000

# Arreglo para almacenar los números cuánticos
arregloEstados = np.ones([nParticulas, 3], np.int)

# Ciclo principal
listaGrafico = []
valorE = 3*nParticulas*(np.pi**2)/2

for k in range(nPasos):
    # Se escoge la partícula a "mover"
    iPart = np.random.randint(nParticulas)
    jNumCuantico = np.random.randint(3)

    # Comentario faltante
    if np.random.random() < 0.5:
        deltaN = +1
        deltaE = (2*arregloEstados[iPart, jNumCuantico]+1)*(np.pi**2)/2
    else:
        deltaN = -1
        deltaE = (-2*arregloEstados[iPart, jNumCuantico]+1)*(np.pi**2)/2

    # Comentario faltante
    if arregloEstados[iPart, jNumCuantico] > 1 or deltaN == 1:
        if np.random.random() < np.exp(-deltaE/temperatura):
            arregloEstados[iPart, jNumCuantico] += deltaN
            valorE += deltaE

    listaGrafico.append(valorE)

# Gráfico
fig, ax = plt.subplots(dpi=120)
ax.plot(listaGrafico)
ax.set_title('Simulación de Monte Carlo de un gas ideal')
ax.set_xlabel('pasos')
ax.set_ylabel('Energía')
# ax.set_ylim([0,500])
# ax.set_xticks(np.arange(0,501,50))
plt.show()
