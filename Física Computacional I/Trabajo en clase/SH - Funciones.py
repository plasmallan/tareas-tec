#########################################################
# Funciones sistema de hormigas PVA

def CargarLocalizaciones(nCiudades):
    """
    Esta función se encarga de cargar las localizaciones de las ciudades o nodos para obtimizar la trayectoria
    Debe retornarse un numpy array que contenga las coordenadas en el formato:
    [[x_0, y_0], [x_1, y_2], ..., [x_N, y_N]]
    """
    listaCoordenadas = []
    for iCiudad in range(nCiudades):
        posX = 0.1*((9+11*(iCiudad**2))%200)
        posY = 0.1*((7+1723*iCiudad)%200)

        listaCoordenadas.append([posX, posY])

    arregloCiudades = np.asarray(listaCoordenadas)
    return arregloCiudades


########################################################
def DistanciaEntreCiudades(nodo1, nodo2, coordCiudades):
    """
    Cálculo de la longitud euclídea entre dos ciudades
    ---
    Entradas:nodo1, nodo2: enteros, índices de la ciudades a calcular; arreglo de coordenadas de las ciudades
    """
    return longitudTrayectoria


#########################################################
def ObtenerLongitudCaminoVecinosMásCercanos(coordenadasCiudades):
    """
    Esta función obtiene la longitud del camino de los vecinos más cercanos
    para las coordenadas de las ciudades utilizadas con una ciudad inicial seleccionada
    aleatoriamente.
    ---
    Entrada: arreglo de coordenadas de las ciudades
    Salida: La función retorna el valor de la longitud del camino de los vecinos más cercanos
    """

    return longitudCaminoMásCercano


#########################################################
def InicializarNivelFeromonas(nCiudades, tau_0):
    """
    Inicialización de los niveles de feromonas de la simulación
    Salidas: arreglo de nCiudades x nCiudades con tau_0 como valor
    inicial de feromona para cada arista
    """
    return nivelFeromonas


#########################################################
def ObtenerVisibilidad(coordCiudades):
    """
    Usa el arreglo de coordenadas de las ciudades para obtener las distancias
    entre todas las ciudades.
    ---
    Salida arregloVisibilidad: matriz simétrica con las distancias entre todas
    las ciudades
    """
    return arregloVisibilidad


#########################################################
def GenerarCamino(nivelFeromonas, visibilidad, alpha, beta):
    """
    Genera la lista de nodos recorridos por la k-ésima hormiga
    """
    return caminoGenerado


##########################################################
def ObtenerProbabilidad(nivelFeromonas, visibilidad, alpha, beta,
                        listaTabú, iNodo, jNodo):
    """
    probabilidad condicional p(Cij|S)
    Retorna la probabilidad de ir del nodo i al nodo j
    """
    return probabilidadNodo


############################################################
def ObtenerNodo(matrizProb):
    """
    Retorna el próximo nodo en el camino
    Salida: índice del próximo nodo
    """
    return próximoNodo


############################################################
def ObtenerLongitudCamino(camino, coordCiudades):
    """
    Obtener la longitud del camino según las coordenadas de las ciudades
    """
    return longitudCamino


############################################################
def CálculoDeltaTau(colecciónCaminos, colecciónLongitudCaminos):
    """
    Calcula la matriz deltaTau
    """

    return deltaTau


##############################################################
def ActualizarNivelFeromonas(nivelFeromonas, deltaNivelFeromonas, rho):
    """
    Actualiza el arreglo nivel feromonas con los valores del arreglo deltaNivelferomonas
    Retorna el nivel de feromonas actualizado
    """
    return nivelFeromonasActualizado
