##################################
# Sistema un hormigas PVA
##################################

import numpy as np

coordCiudades = CargarLocalizaciones()
nCiudades = len(coordenadasCiudades)

##################################
# Parámetros del algoritmo

nHormigas = 20
alpha = 1.0
beta = 5.0
rho = 0.5

# POR HACER: i) escribir la función ObtenerLongitudCaminoVecinosMásCercanos(),
#           ii) asignarle el valor retornado a la variable longitudCaminoVecinosMásCercanos y,
#          iii) definir tau_0 como el número de hormigas entre la longitud del camino de vecinos más cercanos.

# POR HACER: definir la variable longitudMínimaDeseada que sea un valor de referencia para detener la simulación.

#################################
# Inicialización

# POR HACER: i) escribir la función InicializarNivelFeromonas(),
#           ii) asignar a la variable nivelFeromonas el arreglo retornado por InicializarNivelFeromonas()
#          iii) escribir la función ObtenerVisibilidad()
#           iv) asignar a la varible visibilidad el valor retornado por ObtenerVisibilidad()

##################################
# Ciclo Principal

# Se define la variable que va a almacenar la longitud mínima encontrada por iteración
longitudMínima = 1e4

iIteración = 0

while longitudMínima > longitudMínimaDeseada:
    iIteración += 1

    # Se generan los caminos que recorren las hormigas

    colecciónCaminos = []
    colecciónLongitudCaminos = []

    for kHormiga in range(nHormigas):
        # POR HACER: Escribir la función GenerarCamino()
        camino = GenerarCamino(nivelFeromonas, visibilidad, alpha, beta)

        # POR HACER: Escribir la función ObtenerLongitudCamino
        longitudCamino = ObtenerLongitudCamino(camino, coordenadasCiudades)

        if longitudCamino < longitudMínima:
            longitudMínima = longitudCamino
            print('Iteración {}, hormiga {}: longitud del camino más corto = {.5f}'.format(iIteración, kHormiga, longitudMínima)

        colecciónCaminos.append(camino)

        # REVISAR
        colecciónLongitudCaminos.append(longitudCamino)

    # Fin del ciclo sobre las hormigas

    ###############################################
    # Actualización de los niveles de feromonas

    # POR HACER: Escribir la función CálculoDeltaTau()
    deltaNivelFeromonas = CálculoDeltaTau(colecciónCaminos, colecciónLongitudCaminos)

    # POR HACER: Escribir la función ActualizarNivelFeromonas()
    nivelFeromonas = ActualizarNivelFeromonas(nivelFeromonas, deltaNivelFeromonas, rho)
