import numpy as np
from scipy.optimize import minimize


def initialize_data(data, nplanes):
    '''
    Inicializa los datos, en "nplanes" planos seleccionados del intervalo [0, 2pi] 
    
    filename: nombre del archivo a analizar
    '''
      
    torplanes = np.linspace(0, 2*np.pi, num=nplanes, endpoint=False)
    dphi = 0.01
    phi_up = torplanes + dphi/2
    phi_down = torplanes - dphi/2
    R0 = 0.2477 

    boolarr = [np.logical_and(
                    data[:,1] < phi_up[i],
                    data[:,1] > phi_down[i]) for i in range(len(torplanes))]
    
    boolarr = np.array(boolarr).any(axis = 0)
    pos = np.where(boolarr)[0]
    data = data[pos]
    
    R = data[:,0]
    phi = data[:,1]
    z = data[:,2]
    r = data[:,3]
    theta = data[:,4]
    B = data[:,5]
    Br = data[:,6]
    Bphi = data[:,7]
    Bz = data[:,8]

    return np.array([R, phi, z, r, theta,B, Br, Bphi, Bz])

def data_setup(dataset, m, n, nfp):
    '''
    Se construyen las matrices de cosenos y senos para obtener las series de fourier. 
    
    dataset:  
    '''

    rdata, phi, zdata, _,theta, *_ = dataset
    cosmat = np.zeros((m+1,2*n+1,len(rdata)))
    sinmat = np.zeros((m+1,2*n+1,len(zdata)))
    
    for j in range(m+1): 
        for i in range(-n, n+1):
            cosmat[j,i+n] = np.cos(j*theta-i*nfp*phi)
            sinmat[j,i+n] = np.sin(j*theta-i*nfp*phi)
    
    return [rdata, zdata], np.array([cosmat, sinmat]), [phi, theta]

def matsmesh_setup(m,n,n_fp,num):

    phi = np.linspace(0,2*np.pi,num=num)
    tht = np.linspace(0,2*np.pi,num=num)

    p, t = np.meshgrid(phi, tht)

    cosmatpp = np.zeros((m+1,2*n+1,len(phi),len(tht)))
    sinmatpp = np.zeros((m+1,2*n+1,len(phi),len(tht)))

    for j in range(m+1): 
        for i in range(-n, n+1):
            cosmatpp[j,i+n] = np.cos(j*t-i*n_fp*p)
            sinmatpp[j,i+n] = np.sin(j*t-i*n_fp*p)

    return [cosmatpp, sinmatpp], [phi, tht], [p,t]

def errors(data, mats, m, n):
    def square_error_r(par):
        par = np.array(par).reshape(m+1,2*n+1)
        dy = (1/len(data[0]))*(data[0] - np.einsum('ij, ijm->m', par, mats[0],optimize=True))**2
        return np.sum(dy)

    def square_error_s(par):
        par = np.array(par).reshape(m+1,2*n+1)
        dy = (1/len(data[1]))*(data[1] - np.einsum('ij, ijm->m', par,mats[1],optimize=True))**2
        return np.sum(dy)
    return square_error_r, square_error_s

def optim_funcs(funcs, m, n):

    p0 = np.random.rand(m+1, 2*n+1)

    res_r = minimize(funcs[0], p0, method='BFGS', tol=1e-5)
    res_z = minimize(funcs[1], p0, method='BFGS', tol=1e-5)
    rcoefs = res_r.x.reshape(m+1, 2*n+1)
    zcoefs = res_z.x.reshape(m+1, 2*n+1)
    


    return rcoefs, zcoefs

