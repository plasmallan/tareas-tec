from matplotlib.pyplot import *
import numpy as np
import matplotlib.pyplot as plt

n1 = -0.25 - 0.25*1j
n2 = 0
n3 = 0

a = np.array([n1])
fig,ax = subplots()
for i in range(len(a)):

    if i == 2 and a.real[i]>=0:
        x = np.arange(0,a.real[i],0.001)
        plot(x,((a.imag[i])/(a.real[i])*(x - a.real[i])+a.imag[i]),"--",label = "z1 + z2")

    elif i == 2:
        x = np.arange(a.real[i],0,0.001)
        plot(x,((a.imag[i])/(a.real[i])*(x - a.real[i])+a.imag[i]),"--",label = "z1 + z2")

    elif a.real[i]>=0:
        x = np.arange(0,a.real[i],0.001)
        plot(x,((a.imag[i])/(a.real[i])*(x - a.real[i])+a.imag[i]),label = "z"+str(i+1))
    else:
        x = np.arange(a.real[i],0,0.001)
        plot(x,((a.imag[i])/(a.real[i])*(x - a.real[i])+a.imag[i]),label = "z"+str(i+1))

ax.scatter(a.real,a.imag)
plt.xlabel("Parte Real")
plt.ylabel("Parte Imaginaria")
plt.legend()
##ax.spines['left'].set_position('center')
##ax.spines['bottom'].set_position('center')
##ax.spines['right'].set_color('none')
##ax.spines['top'].set_color('none')
plt.show()
